package com.solomia.polinom;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MyClass {
    private static String doReverse(String str) {
        String reverseWord = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            reverseWord += str.charAt(i);
        }
        return reverseWord;
    }

    public static void findPolindrom(String word) {
        String reverseWord = doReverse(word);
        if (reverseWord.equals(word)) {
            System.out.println("Polindrom: " + word);
        }
    }

    public static void findDoubling(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (i != word.length() - 1 && word.charAt(i) == word.charAt(++i)) {
                System.out.println("Doubling in word: " + word);
                --i;
            } 
        }
    }

    public static void main(String[] args) {
//        String sentence = "Dad is very busyy and my mummy, too";
//        String[] words = sentence.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
//        Arrays.stream(words)
//                .mapToObj(i->char(i))
//        for (String s : words) {
//            findPolindrom(s);
//            findDoubling(s);
//        }
//        List<String> list = Arrays.asList("a1", "a2", "a3", "a1");
//        String max = list.stream().max(String::compareTo).get();
//        System.out.println(max);
//        StringBuilder b = new StringBuilder(); // метод_инициализации_аккумулятора
//        for(String str: list) {
//            b.append(str).append(" , "); // метод_обработки_каждого_элемента,
//        }
//        String joinBuilderOld = b.toString();
//        System.out.println(joinBuilderOld);
//
//        String joinBuilder = list.stream().collect(
//                Collector.of(
//                        StringBuilder::new,
//                        (bb ,s) -> bb.append(s).append(" , "),
//                        (b1, b2) -> b1.append(b2).append(" , "),
//                        StringBuilder::toString
//                )
//        );
//        System.out.println(joinBuilder);
//
//        List<Integer> integerList = Arrays.asList(1,4,6,2,7,8);
//        integerList.stream()
//                .filter(i->i%2==0)
//                .forEach(i->System.out.print(i+ " "));
//        int sum = integerList.stream()
//                .reduce((s1,s2)->s1+s2)
//                .orElse(-1);
//        System.out.println(sum);


    }
}
