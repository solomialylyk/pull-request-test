package com.solomia.YANA;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Combinator {
    private int m;
    private int dimension;
    private int[] state;
    private int index = 1;

    public Combinator(int n) {
        dimension = n;
        state = new int[n];
        for (int i = 0; i < state.length; i++) {
            state[i] = 0;
        }
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getM() {
        return m;
    }

    public boolean next() {
        index++;
        return move(dimension - 1);
    }

    private boolean move(int index) {
        if (state[index] < dimension - 1) {
            state[index]++;
            return true;
        }

        state[index] = 0;
        if (index == 0) {
            return false;
        } else {
            return move(index - 1);
        }
    }

    public int getIndex() {
        return index;
    }

    public boolean isPeace() {
        for (int i = 0; i < state.length; i++) {
            for (int j = i + 1; j < state.length; j++) {
                if (state[i] == state[j]) { // перевірка чи битиме ферзь по вертикалі
                    return false;
                }
                if (Math.abs(i - j) == Math.abs(state[i] - state[j])) {  // перевірка чи битиме ферзь по діагоналі
                    return false;
                }
            }
        }

        return true;
    }

    public void printState() {
        for (int i = 0; i < state.length; i++) {
            int position = state[i];
            for (int j = 0; j < dimension; j++) {
                System.out.print(j == position ? 'X' : '_');
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Combinator c = new Combinator(8);
        int maxState = 11*8 + 1*4; //максимальна кількість можливих розстановок
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input m (number of combination)");
        c.setM(scanner.nextInt());
        if (c.getM() > maxState) {
            c.setM(maxState);
        }
        ExecutorService fixedExecutorService = Executors.newFixedThreadPool(10);
        fixedExecutorService.submit(() -> {
        int counter = 0;
        do {
            if (c.isPeace()) {
                if (counter < c.getM()) {
                    counter++;
                    c.printState();
                    System.out.println("----------------------");
                }
            }
        } while (c.next());
        System.out.println("Result state: " + counter);
    });
        fixedExecutorService.shutdownNow();
    }
}