package com.solomia.YANA;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class laba4 {

    public static int[][] fillingMatrix(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                mas[i][j] = (int) (Math.random() * 20);
            }
        }
        return mas;
    }

    public static void printMatrix(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printVector(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            System.out.print(vector[i] + " ");
        }
        System.out.println();
    }

    public static int[] multiplyMatrixOnVector(int[][] mas, int[] vector) {
        int res[] = new int[mas.length];
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                res[i] += mas[i][j] + res[i];
            }
        }
        return res;
    }

    public static int[] fillingB1(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (i % 2 == 0) {
                vector[i] = 24/(i*i+4);
            } else {
                vector[i] =  24;
            }
        }
        return vector;
    }

    public static int[] fillingC1(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            vector[i] = (int) (Math.random() * 20);//-10
        }
        return vector;
    }

    public static int[] addVectorToVector(int[] vector1, int[] vector2) {
        int res[] = new int[vector1.length];
        for (int i = 0; i < vector1.length; i++) {
            res[i] = vector1[i] + vector2[i];
        }
        return res;
    }

    public static int[] subtractVectorToVector(int[] vector1, int[] vector2) {
        int res[] = new int[vector1.length];
        for (int i = 0; i < vector1.length; i++) {
            res[i] = vector1[i] - vector2[i];
        }
        return res;
    }

    public static int[][] fillingC2(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                if (i+3*j*j == 0) {
                    mas[i][j] = 0;
                } else {
                    mas[i][j] = 24/(i+3*j*j);
                }

            }
        }
        return mas;
    }

    public static int[][] multiplyMatrixOnMatrix(int[][] mas1, int[][] mas2) {
        int res[][] = new int[mas1.length][mas2.length];
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas2[0].length; j++) {
                for (int k = 0; k < mas2.length; k++) {
                    res[i][j] += mas1[i][k] * mas2[k][i];
                }
            }
        }
        return res;
    }

    public static int multiplyVectorOnVector(int[] vector1, int[] vector2) {
        int res = 0;
        for (int i = 0; i < vector1.length; i++) {
            res += vector1[i] * vector2[i];
        }
        return res;
    }


    public static int[][] addMatrixToMatrix(int[][] mas1, int[][] mas2) {
        int res[][] = new int[mas1.length][mas1.length];
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas1.length; j++) {
                res[i][j] = mas1[i][j] + mas2[i][j];
            }
        }
        return res;
    }

    public static int[] multiplyVectorOnNumber(int[] vector, int number) {
        int res[] = new int[vector.length];
        for (int i = 0; i < vector.length; i++) {
            res[i] = vector[i] * number;
        }
        return res;
    }

    public static int[] addVectorOnNumber(int[] vector, int number) {
        int res[] = new int[vector.length];
        for (int i = 0; i < vector.length; i++) {
            res[i] = vector[i] + number;
        }
        return res;
    }

    public static int[][] multiplyMatrixOnNumber(int[][] vector, int number) {
        int res[][] = new int[vector.length][vector.length];
        for (int i = 0; i < vector.length; i++) {
            for (int j=0; j<vector.length; j++) {
                res[i][j] = vector[i][j] * number;
            }
        }
        return res;
    }

    public static void main(String[] args) throws IOException {
        Thread thread = new Thread(() -> {
            System.out.println("Input n = ");
            Scanner scan = new Scanner(System.in);
            int n = scan.nextInt();

            System.out.println("1)");
            int A[][] = new int[n][n];
            A = fillingMatrix(A);
            int b[] = new int[n];
            b = fillingB1(b);
            System.out.print("y1 = ");
            int y1[] = multiplyMatrixOnVector(A, b);
            printVector(y1);
            System.out.println("2)");
            int c1[] = new int[n];
            c1 = fillingC1(c1);
            int temp[] = multiplyVectorOnNumber(c1, 24);
            int temp1[] =subtractVectorToVector(b, temp);
            int y2[] = multiplyMatrixOnVector(A, temp1);
            System.out.print("y2 = ");
            printVector(y2);
            System.out.println("3)");
            int c2[][] = new int[n][n];
            c2 = fillingC2(c2);
            int B2[][] = new int[n][n];
            B2 = fillingMatrix(B2);
            int temp3[][] = multiplyMatrixOnNumber(c2, 24);
            int temp4[][]= addMatrixToMatrix(B2, temp3);
            System.out.println("y3 = ");
            int y3[][] = multiplyMatrixOnMatrix(A, temp4);
            printMatrix(y3);
            int x1 = multiplyVectorOnVector(multiplyMatrixOnVector(multiplyMatrixOnMatrix(y3, y3), y2), y1);
            int[] x2 = addVectorOnNumber(y1, x1);
            int[] x3 = addVectorToVector(multiplyMatrixOnVector(y3, y1), y2);
            int x = multiplyVectorOnVector(x2, x3);
            System.out.println(" x= "+ x);
        });
        thread.start();
    }
}
