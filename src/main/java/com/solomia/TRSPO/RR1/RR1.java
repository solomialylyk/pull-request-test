package com.solomia.TRSPO.RR1;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

public class RR1 {

    public static int[][] fillingMatrix(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                mas[i][j] = (int) (1+Math.random() * 20);//-10
            }
        }
        return mas;
    }

    public static void printMatrix(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printVector(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            System.out.print(vector[i] + " ");
        }
        System.out.println();
    }

    public static int[] multiplyMatrixOnVector(int[][] mas, int[] vector) {
        int res[] = new int[mas.length];
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                res[i] += mas[i][j] + res[i];
            }
        }
        return res;
    }

    public static int[] fillingB1(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
                vector[i] = 10/(i*i+1);
                if(vector[i] <= 0) {
                    vector[i]=1;
                }
        }
        return vector;
    }

    public static int[] MultiplyVectorOnNumber(int[] vector, int number) {
        for (int i = 0; i < vector.length; i++) {
            vector[i] = vector[i] * number;
        }
        return vector;
    }

    public static int[] fillingC1(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            vector[i] = (int) (1+Math.random() * 20);//-10
        }
        return vector;
    }

    public static int[] addVectorToVector(int[] vector1, int[] vector2) {
        int res[] = new int[vector1.length];
        for (int i = 0; i < vector1.length; i++) {
            res[i] = vector1[i] + vector2[i];
        }
        return res;
    }

    public static int[][] fillingC2(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                if ((i  + 2* j) == 0) {
                    mas[i][j] = 1;
                } else {
                    mas[i][j] = 1 / (i  + 2* j);
                }
            }
        }
        return mas;
    }

    public static int[][] multiplyMatrixOnMatrix(int[][] mas1, int[][] mas2) {
        int res[][] = new int[mas1.length][mas2.length];
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas2[0].length; j++) {
                for (int k = 0; k < mas2.length; k++) {
                    res[i][j] += mas1[i][k] * mas2[k][i];
                }
            }
        }
        return res;
    }

    public static int multiplyVectorOnVector(int[] vector1, int[] vector2) {
        int res = 0;
        for (int i = 0; i < vector1.length; i++) {
            res += vector1[i] * vector2[i];
        }
        return res;
    }


    public static int[][] addMatrixToMatrix(int[][] mas1, int[][] mas2) {
        int res[][] = new int[mas1.length][mas1.length];
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas1.length; j++) {
                res[i][j] = mas1[i][j] + mas2[i][j];
            }
        }
        return res;
    }

    public static int[][] transpositionMatrix(int[][] mas) {
        int res[][] = new int[mas.length][mas.length];
        for (int i = 0; i < mas.length; i++) {
            for (int j = i + 1; j < mas.length; j++) {
                int temp = mas[i][j];
                mas[i][j] = mas[j][i];
                mas[j][i] = temp;
            }
        }
        return res;
    }

    public void writeToFile(String string) {
        try(FileOutputStream fos=new FileOutputStream("C://SomeDir//notes.txt"))
        {
            byte[] buffer = string.getBytes();
            fos.write(buffer, 0, buffer.length);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("The file has been written");
    }

    public static int[] multiplyVectorOnNumber(int[] vector, int number) {
        int res[] = new int[vector.length];
        for (int i = 0; i < vector.length; i++) {
            res[i] = vector[i] * number;
        }
        return res;
    }

    public static int[][] multiplyMatrixOnNumber(int[][] matrix, int number) {
        int res[][] = new int[matrix.length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j =0; j < matrix.length; j++) {
                res[i][j] = matrix[i][j] * number;
            }
        }
        return res;
    }

    private static int n;
    private static int[] y1 =new int[n];
    private static int[] y2 =new int[n];
    private static int[][] y3 =new int[n][n];
    private int x = 1;
    private final ReentrantLock lock= new ReentrantLock();

    public void setX(int x) {
        this.x = x;
    }

    public void show() {
        System.out.println("Input n");
        Scanner scan = new Scanner(System.in);
        n = scan.nextInt();
        Thread first = new Thread(printFirst());
        Thread second = new Thread(printSecond());
        Thread third = new Thread(printThird());
        first.start();
        second.start();
        third.start();
        Thread fourth = new Thread(printFourth());
        try {
            Thread.sleep(200);
            fourth.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Runnable printFirst() {
        return () -> {
            lock.lock();
            try {
        System.out.println("-----First Task-----");
        int A[][] = new int[n][n];
        A = fillingMatrix(A);
        printMatrix(A);
        int b[] = new int[n];
        System.out.print("b = ");
        b = fillingB1(b);
        printVector(b);
        System.out.print("y1 = ");
        y1= multiplyMatrixOnVector(A, b);
        printVector(y1);
            } finally {
                lock.unlock();
            }

        };
    }

    private Runnable printSecond() {
        return () -> {
            lock.lock();
            try {
                System.out.println("-----Second Task-----");
        int A1[][] = new int[n][n];
        A1 = fillingMatrix(A1);
        printMatrix(A1);
        int c1[] = new int[n];
        c1 = fillingC1(c1);
        System.out.print("c1 = ");
        printVector(c1);
        int b1[] = new int[n];
        System.out.print("b1 = ");
        b1 = fillingB1(b1);
        printVector(b1);
        int temp[] = addVectorToVector(b1, c1);
        y2 = multiplyMatrixOnVector(A1,temp);
        System.out.print("y2 = ");
        printVector(y2);
            } finally {
                lock.unlock();
            }

        };
    }

    private Runnable printThird() {
        return () -> {
            lock.lock();
            try {
                        System.out.println("-----Third Task-----");
        int A2[][] = new int[n][n];
        A2 = fillingMatrix(A2);
        printMatrix(A2);
        int c2[][] = new int[n][n];
        c2 = fillingC2(c2);
        int B2[][] = new int[n][n];
        B2 = fillingMatrix(B2);
        int num =2;
        int temp1[][] = addMatrixToMatrix(c2, multiplyMatrixOnNumber(B2, num));
        System.out.println("y3 = ");
        int y3[][] = multiplyMatrixOnMatrix(A2, temp1);
        printMatrix(y3);

            } finally {
                lock.unlock();
            }

        };
    }

    private Runnable printFourth() {
        return () -> {
            lock.lock();
            try {
                Thread.sleep(2000);
                int []x1 =  multiplyMatrixOnVector(multiplyMatrixOnNumber(y3,multiplyVectorOnVector(y1, y2)), y2);
                printVector(x1);
                System.out.println(x1.length);
                int[] x2 = multiplyMatrixOnVector(multiplyMatrixOnMatrix(multiplyMatrixOnMatrix(y3, y3), y3),y1);
                int[] x3 = addVectorToVector(x2, multiplyMatrixOnVector(y3,y1));
                int[] x4 = addVectorToVector(multiplyMatrixOnVector(y3,y1),  multiplyVectorOnNumber(y2,multiplyVectorOnVector(y2,y1)));

                x = multiplyVectorOnVector(x3, x4);
                System.out.println(" x= " + x);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }

        };
    }

    public static void doTask() {
        Thread thread= new Thread(()-> {
            String nameThread = Thread.currentThread().getName();
            System.out.println("Введіть n");
            Scanner scan = new Scanner(System.in);
            int n = scan.nextInt();

            System.out.println("-----First Task-----");
            int A[][] = new int[n][n];
            A = fillingMatrix(A);
            printMatrix(A);
            int b[] = new int[n];
            System.out.print("b = ");
            b = fillingB1(b);
            printVector(b);
            System.out.print("y1 = ");
            int y1[] = multiplyMatrixOnVector(A, b);
            printVector(y1);

            System.out.println("-----Second Task-----");
            int A1[][] = new int[n][n];
            A1 = fillingMatrix(A1);
            printMatrix(A1);
            int c1[] = new int[n];
            c1 = fillingC1(c1);
            System.out.print("c1 = ");
            printVector(c1);
            int b1[] = new int[n];
            System.out.print("b1 = ");
            b1 = fillingB1(b1);
            printVector(b1);
            int temp[] = addVectorToVector(b1, c1);
            int y2[] = multiplyMatrixOnVector(A1,temp);
            System.out.print("y2 = ");
            printVector(y2);

            System.out.println("-----Third Task-----");
            int A2[][] = new int[n][n];
            A2 = fillingMatrix(A2);
            printMatrix(A2);
            int c2[][] = new int[n][n];
            c2 = fillingC2(c2);
            int B2[][] = new int[n][n];
            B2 = fillingMatrix(B2);
            int num =2;
            int temp1[][] = addMatrixToMatrix(c2, multiplyMatrixOnNumber(B2, num));
            System.out.println("y3 = ");
            int y3[][] = multiplyMatrixOnMatrix(A2, temp1);
            printMatrix(y3);

            int[] x1 =multiplyMatrixOnVector(multiplyMatrixOnNumber(y3,multiplyVectorOnVector(y1, y2)), y2);
            int[] x2 = multiplyMatrixOnVector(multiplyMatrixOnMatrix(multiplyMatrixOnMatrix(y3, y3), y3),y1);
            int[] x3 = addVectorToVector(x2, multiplyMatrixOnVector(y3,y1));
            int[] x4 = addVectorToVector(multiplyMatrixOnVector(y3,y1),  multiplyVectorOnNumber(y2,multiplyVectorOnVector(y2,y1)));

            int x = multiplyVectorOnVector(x3, x4);
            System.out.println(" x= " + x);
        });
        thread.start();
    }
}
