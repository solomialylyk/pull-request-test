package com.solomia.TRSPO.labas;

import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

public class laba5 {
    private int m;
    private int n;
    private int mas[][];
    private final ReentrantLock lock= new ReentrantLock();

    public laba5(int m, int n, int[][] mas) {
        this.m = m;
        this.n = n;
        this.mas = mas;
    }

    public static int[][] fillingRandom(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[0].length; j++) {
                mas[i][j] = (int) (Math.random() * 4);//-10
            }
        }
        return mas;
    }

    private Runnable printFirst() {
        return () -> {
            lock.lock();
            try {
                fillingRandom(mas);
            } finally {
                lock.unlock();
            }

        };
    }

    private Runnable printSecond() {
        return () -> {
            lock.lock();
            try {
                Thread.sleep(200);
                printMatrix(mas);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }

        };
    }

    private Runnable printThird() {
        return () -> {
            lock.lock();
            try {
                Thread.sleep(1000);
                findFour(mas);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }

        };
    }

    public void doTask() {
        Thread first = new Thread(printFirst());
        Thread second = new Thread(printSecond());
        Thread third = new Thread(printThird());
        first.start();
        second.start();
        third.start();
    }

    public static int[][] fillingConsole(int [][]mas) {
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[0].length; j++) {
                mas[i][j] = scan.nextInt();
            }
        }
        return mas;
    }

    public static void printMatrix(int[][] mas) {
        System.out.println("Matrix");
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[0].length; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void findFour(int [][]mas) {
        int count=0;
        for (int i =0; i< mas.length; i++) {
            for (int j = 0; j< mas[0].length; j++) {
                if(i!=0 && j != mas[0].length-1 ) {
                    int Aij = mas[i][j];
                    int Aijj = mas[i][++j];
                    --j;
                    int Aiij = mas[--i][j];
                    ++i;
                    int Aiijj= mas[--i][++j];
                    ++i;
                    --j;
                    if(Aij == Aiij && Aij ==Aijj && Aij==Aiijj) {
                        count++;
                    }

                }
            }
        }
        System.out.println("Result = "+count);
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input n");
        int n=scanner.nextInt();
        System.out.println("Input m");
        int m=scanner.nextInt();
        laba5 laba= new laba5(n,m,new int[n][m]);
        laba.doTask();
//        System.out.println(A.length); // стовпці і
//        System.out.println(A[0].length); // рядки j
//        A = fillingRandom(A);
//        printMatrix(A);
//        findFour(A);
        //new laba5().doTask();
    }
}
