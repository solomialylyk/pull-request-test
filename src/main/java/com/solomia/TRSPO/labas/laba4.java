package com.solomia.TRSPO.labas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class laba4 {

    public static int[][] fillingMatrix(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                mas[i][j] = (int) (Math.random() * 20);//-10
            }
        }
        return mas;
    }

    public static void printMatrix(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printVector(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            System.out.print(vector[i] + " ");
        }
        System.out.println();
    }

    public static int[] multiplyMatrixOnVector(int[][] mas, int[] vector) {
        int res[] = new int[mas.length];
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                res[i] += mas[i][j] + res[i];
            }
        }
        return res;
    }

    public static int[] fillingB1(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (i % 2 == 0) {
                vector[i] = i;
            } else {
                vector[i] = 15 / i;
            }
        }
        return vector;
    }

    public static int[] MultiplyVectorOnNumber(int[] vector, int number) {
        for (int i = 0; i < vector.length; i++) {
            vector[i] = vector[i] * number;
        }
        return vector;
    }

    public static int[] fillingC1(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            vector[i] = (int) (Math.random() * 20);//-10
        }
        return vector;
    }

    public static int[] addVectorToVector(int[] vector1, int[] vector2) {
        int res[] = new int[vector1.length];
        for (int i = 0; i < vector1.length; i++) {
            res[i] = vector1[i] + vector2[i];
        }
        return res;
    }

    public static int[][] fillingC2(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                if (i * i - j == 0) {
                    mas[i][j] = 0;
                } else {
                    mas[i][j] = 15 / (i * i - j);
                }

            }
        }
        return mas;
    }

    public static int[][] multiplyMatrixOnMatrix(int[][] mas1, int[][] mas2) {
        int res[][] = new int[mas1.length][mas2.length];
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas2[0].length; j++) {
                for (int k = 0; k < mas2.length; k++) {
                    res[i][j] += mas1[i][k] * mas2[k][i];
                }
            }
        }
        return res;
    }

    public static int multiplyVectorOnVector(int[] vector1, int[] vector2) {
        int res = 0;
        for (int i = 0; i < vector1.length; i++) {
            res += vector1[i] * vector2[i];
        }
        return res;
    }


    public static int[][] addMatrixToMatrix(int[][] mas1, int[][] mas2) {
        int res[][] = new int[mas1.length][mas1.length];
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas1.length; j++) {
                res[i][j] = mas1[i][j] + mas2[i][j];
            }
        }
        return res;
    }

    public static int[][] transpositionMartix(int[][] mas) {
        int res[][] = new int[mas.length][mas.length];
        for (int i = 0; i < mas.length; i++) {
            for (int j = i + 1; j < mas.length; j++) {
                int temp = mas[i][j];
                mas[i][j] = mas[j][i];
                mas[j][i] = temp;
            }
        }
        return res;
    }

    public void writeToFile(String string) {
        try(FileOutputStream fos=new FileOutputStream("C://SomeDir//notes.txt"))
        {
            byte[] buffer = string.getBytes();
            fos.write(buffer, 0, buffer.length);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("The file has been written");
    }

    public static int[] multiplyVectorOnNumber(int[] vector, int number) {
        int res[] = new int[vector.length];
        for (int i = 0; i < vector.length; i++) {
            res[i] = vector[i] * number;
        }
        return res;
    }

    public static void show() throws IOException {
        Thread thread = new Thread(() -> {
            String nameThread = Thread.currentThread().getName();
            System.out.println("Input n");
            Scanner scan = new Scanner(System.in);
            int n = scan.nextInt();

            System.out.println("-----First Task-----");
            int A[][] = new int[n][n];
            A = fillingMatrix(A);
            printMatrix(A);
            int b[] = new int[n];
            System.out.print("b = ");
            b = fillingB1(b);
            printVector(b);
            System.out.print("y1 = ");
            int y1[] = multiplyMatrixOnVector(A, b);
            printVector(y1);

            System.out.println("-----Second Task-----");
            int num = 15;
            int temp[] = MultiplyVectorOnNumber(y1, num);
            int c1[] = new int[n];
            c1 = fillingC1(c1);
            System.out.print("c1 = ");
            printVector(c1);
            int y2[] = addVectorToVector(temp, c1);
            System.out.print("y2 = ");
            printVector(y2);

            System.out.println("-----Third Task-----");
            int c2[][] = new int[n][n];
            c2 = fillingC2(c2);
            int B2[][] = new int[n][n];
            B2 = fillingMatrix(B2);
            int temp1[][] = multiplyMatrixOnMatrix(A, c2);
            System.out.println("y3 = ");
            int y3[][] = addMatrixToMatrix(temp1, B2);
            printMatrix(y3);

            int[] x1 = addVectorToVector(y2, y1);
            int[] x2 = multiplyMatrixOnVector(multiplyMatrixOnMatrix(y3, y3), y1);
            int[] x3 = multiplyMatrixOnVector(y3, multiplyVectorOnNumber(y1, multiplyVectorOnVector(y2, y2)));
            int[] x = addVectorToVector(addVectorToVector(x1, x2), x3);
            System.out.println(" x= ");
            printVector(x);
        });
        thread.start();
    }

    public static void main(String[] args) throws IOException {
        show();
    }
}
