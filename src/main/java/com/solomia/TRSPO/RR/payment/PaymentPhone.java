package com.solomia.TRSPO.RR.payment;

public class PaymentPhone implements Payment {
    final private double price= 40;
    final private String name ="Phone";

    public PaymentPhone() {
    }

    @Override
    public double getCost() {
        return price;
    }

    @Override
    public String getName() {
        return name;
    }
}
