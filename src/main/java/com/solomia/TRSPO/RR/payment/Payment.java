package com.solomia.TRSPO.RR.payment;

public interface Payment {
    double getCost();
    String getName();
}
