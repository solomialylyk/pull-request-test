package com.solomia.TRSPO.RR.payment;

public class PaymentGas implements Payment {
    final private double price= 20;
    final private String name ="Gas";

    public PaymentGas() {
    }

    @Override
    public double getCost() {
        return price;
    }

    @Override
    public String getName() {
        return name;
    }
}
