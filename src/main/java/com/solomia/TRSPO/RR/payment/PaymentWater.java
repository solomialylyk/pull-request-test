package com.solomia.TRSPO.RR.payment;

public class PaymentWater implements Payment {
    final private double price= 7.8;
    final private String name ="Cold Water";

    public PaymentWater() {
    }

    @Override
    public double getCost() {
        return price;
    }

    @Override
    public String getName() {
        return name;
    }
}
