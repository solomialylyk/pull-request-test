package com.solomia.TRSPO.RR;

import com.solomia.TRSPO.RR.gui.FormGUI;

public class Main {
    public static void main(String[] args) {
        FormGUI app = new FormGUI();
        app.setVisible(true);
    }
}
