package com.solomia.TRSPO.RR.gui;

import com.solomia.TRSPO.RR.payment.Payment;
import com.solomia.TRSPO.RR.payment.PaymentGas;
import com.solomia.TRSPO.RR.payment.PaymentPhone;
import com.solomia.TRSPO.RR.payment.PaymentWater;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import static java.lang.Math.random;

public class FormGUI extends JFrame{
    private JButton button = new JButton("Print");
    private JTextField input = new JTextField("", 5);
    private JTextField input1 = new JTextField("", 5);
    private JLabel label = new JLabel("Recipient's details: ");
    private JLabel label1 = new JLabel("Payer's details: ");
    private JRadioButton radioButton = new JRadioButton("Payment water");
    private JRadioButton radioButton1 = new JRadioButton("Payment gas");
    private JRadioButton radioButton2 = new JRadioButton("Payment phone");
    private JRadioButton radioButton3 = new JRadioButton("Payment all");
    private JCheckBox checkBox = new JCheckBox("Agree", false);

    public FormGUI() {
        super("Payment Form");
        this.setBounds(100, 100, 500, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(3,2,2,2));
        container.add(label);
        container.add(input);
        container.add(label1);
        container.add(input1);

        ButtonGroup group = new ButtonGroup();
        group.add(radioButton);
        group.add(radioButton1);
        group.add(radioButton2);
        group.add(radioButton3);
        container.add(radioButton);
        radioButton.setSelected(true);
        container.add(radioButton1);
        container.add(radioButton2);
        container.add(radioButton3);
        container.add(checkBox);
        button.addActionListener(new ButtonEventListener());
        container.add(button);
    }

    class ButtonEventListener implements ActionListener {
        private String paymentWater() {
            Payment paymentWater = new PaymentWater();
            int waterCube= (int)(1+ random() *10);
            return paymentWater.getName()+ ": " + paymentWater.getCost() * waterCube + " UAN\n";
        }

        private String  paymentGas () {
            int gasCube = (int)(1+ random() *10);
            Payment paymentGas = new PaymentGas();
            return  paymentGas.getName() + ": " + paymentGas.getCost()*gasCube+ " UAN\n";
        }

        private String  paymentPhone() {
            Payment paymentPhone = new PaymentPhone();
            return paymentPhone.getName() + ": " + paymentPhone.getCost() + " UAN\n";
        }
        public void actionPerformed(ActionEvent e) {
            String message = "";
            message += "The recipient's details " +input.getText() + "\n";
            message += "The payer's details " + input1.getText() + "\n";
            if(radioButton.isSelected()) {
                message += paymentWater();
            } else if (radioButton1.isSelected()) {
                message += paymentGas();
            } else if (radioButton2.isSelected()) {
                message += paymentPhone();
            } else {
                message += paymentWater()+ paymentGas() + paymentPhone();
            }
            JOptionPane.showMessageDialog(null, message, "Preview", JOptionPane.INFORMATION_MESSAGE);

        }
    }

}
